Ce git permet de déployer sur Microsoft Azure Cloud:
    1- Un ressource Group
    2- Un Virtual Network
    3- Un Subnet
    4- Un NsG Network Security Group
    5- Deux régles de securité
    6- Un Cluster Kubernetes (AKS)
Ensuite en deuxieme étape il va déployer plusieurs ressources à l'intérieur du Cluster AKS crée précédemment, voici la liste :

    1- Quatre namespaces (prometheus, cortex, thehive, wazuh)
    2- Une Charte Helm cortex dans le namespace cortex
    3- Une Charte Helm thehive dans le namespace thehive
    4- Une Charte Helm wazuh dans le namespace wazuh
    5- Une Charte Helm alertmanager dans le namespace prometheus
    6- Une Charte Helm prometheus dans le namespace prometheus

Grâce aux différentes Chartes nous allons pourvoir configurer tous les outils ensembles pour qu'il forment notre plateforme SOC (Security Operation Center)