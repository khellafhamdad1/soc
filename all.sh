#!/bin/bash
echo "Lancement du Terraform Init pour récuperer les différents provider"
terraform init

echo "Lancement du plan ce qui nous permet d'avoir une idée sur ce qui sera créer aprés l'apply"
terraform plan

echo "Création des ressources en cours"
terraform apply -auto-approve

# Copie du kubeconfig de notre cluster vers le fichier soc-supdevinci-kubeconfig
terraform output kube_config > soc-supdevinci-kubeconfig
export KUBE_CONFIG=${pwd}/soc-supdevinci-kubeconfig

echo "Affichage du Kube Config de notre cluster crée"
cat $KUBE_CONFIG

echo "Affichage de toutes les ressources kubernetes dans les différents Namespaces disponibles"
kubectl get all --all-namespaces --kubeconfig ${KUBE_CONFIG}

echo "Affichage des Namespaces crées"
kubectl get ns --kubeconfig ${KUBE_CONFIG}

echo "Affichage des ressources dans le namespace Cortex"
kubectl get svc -n cortex --kubeconfig ${KUBE_CONFIG}

echo "Affichage des ressources dans le namespace Wazuh"
kubectl get svc -n wazuh --kubeconfig ${KUBE_CONFIG}

echo "Affichage des ressources dans le namespace The Hive"
kubectl get svc -n thehive --kubeconfig ${KUBE_CONFIG}

echo "Affichage des ressources dans le namespace Prometheus"
kubectl get svc -n prometheus --kubeconfig ${KUBE_CONFIG}

echo "Affichage des ressources dans le namespace Grafana"
kubectl get svc -n grafana --kubeconfig ${KUBE_CONFIG}

echo "Affichage des ressources dans le namespace Default"
kubectl get svc -n default --kubeconfig ${KUBE_CONFIG}

echo "Déploiement du SOC (Security Operations Center) a été accompli avec succès"
