########################################################################################################################
######### AZURE Kubernetes Services AKS ################################################################################
########################################################################################################################
resource "azurerm_resource_group" "rg" {
  name     = "rg-soc-supdevinci"
  location = "West Europe"
}

resource "azurerm_virtual_network" "vnet" {
  name                = "vnet-soc-supdevinci"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "subnet-soc-supdevinci"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_security_group" "nsg" {
  name                = "nsg-soc-supdevinci"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_network_security_rule" "allow_ssh" {
  name                        = "allow_ssh"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_network_security_rule" "allow_http" {
  name                        = "allow_http"
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                = "aks-soc-supdecvinci"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = "aks-soc-supdevinci"

  default_node_pool {
    name            = "default"
    node_count      = 3
    vm_size         = "Standard_DS2_v2"
    os_disk_size_gb = 30
    vnet_subnet_id  = azurerm_subnet.subnet.id
  }

  identity {
    type = "SystemAssigned"
  }

  network_profile {
    network_plugin     = "azure"
    network_policy     = "calico"
    load_balancer_sku  = "Standard"
    outbound_type      = "loadBalancer"
    service_cidr       = "100.64.0.0/16"
    dns_service_ip     = "100.64.0.10"
    docker_bridge_cidr = "172.17.0.1/16"
    pod_cidr           = "10.244.0.0/16"
  }
  tags = {
    environment = "prod"
  }
}
resource "azurerm_public_ip" "aks_lb_public_ip" {
  name                = "aks-lb-public-ip"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"

  tags = {
    environment = "prod"
  }
}


########################################################################################################################
######### Kubernetes Ressources K8S ####################################################################################
########################################################################################################################

resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = "prometheus"
  }
}
resource "kubernetes_namespace" "cortex" {
  metadata {
    name = "cortex"
  }
}
resource "kubernetes_namespace" "thehive" {
  metadata {
    name = "thehive"
  }
}
resource "kubernetes_namespace" "wazuh" {
  metadata {
    name = "wazuh"
  }
}
resource "kubernetes_namespace" "grafana" {
  metadata {
    name = "grafana"
  }
}

########################################################################################################################
######### HELM Resources ###############################################################################################
########################################################################################################################
resource "helm_release" "wazuh" {
  name       = "wazuh"
  namespace  = kubernetes_namespace.wazuh.metadata[0].name
  repository = "${path.module}/wazuh"
  #   chart      = "wazuh"
  #   version    = "3.13.2"
  values = [
    "${file("${path.module}/wazuh/values.yaml")}"
  ]
}
resource "helm_release" "cortex" {
  name       = "cortex"
  namespace  = kubernetes_namespace.cortex.metadata[0].name
  repository = "${path.module}/cortex"
  #   chart      = "cortex"
  #   version    = "0.4.0"
  values = [
    "${file("./cortex/values.yaml")}"
  ]
}

resource "helm_release" "thehive" {
  name       = "thehive"
  namespace  = kubernetes_namespace.thehive.metadata[0].name
  repository = "${path.module}/thehive"
  #   chart      = "thehive"
  #   version    = "4.1.0"
  values = [
    "${file("${path.module}/thehive/values.yaml")}"
  ]
}

resource "helm_release" "alertmanager" {
  name       = "alertmanager"
  namespace  = kubernetes_namespace.prometheus.metadata[0].name
  repository = "${path.module}/alertmanager"
  values = [
    "${file("${path.module}/alertmanager/values.yaml")}"
  ]
}


resource "helm_release" "prometheus" {
  name       = "prometheus"
  namespace  = kubernetes_namespace.prometheus.metadata[0].name
  repository = "${path.module}/prometheus"
  #   chart      = "prometheus"
  #   version    = "14.6.1"
  values = [
    "${file("${path.module}/prometheus/values.yaml")}"
  ]

  set {
    name  = "serverFiles.prometheus.yml"
    value = file("${path.module}/prometheus/config.yaml")
  }
}


resource "helm_release" "grafana" {
  name       = "grafana"
  namespace  = kubernetes_namespace.grafana.metadata[0].name
  repository = "${path.module}/grafana"
  values = [
    "${file("./grafana/values.yaml")}"
  ]
}
