output "client_certificate" {
  description = "Client certificate to be used by kubectl and helm"
  value       = azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate
}

output "kube_config" {
  description = "Kube config to be used by kubectl and helm"
  value       = azurerm_kubernetes_cluster.aks.kube_config_raw
}

output "cortex_endpoint" {
  description = "The Cortex Endpoint"
  value       = helm_release.cortex.outputs.0.endpoint
}

output "the_hive_endpoint" {
  description = "The Hive Endpoint"
  value       = helm_release.the_hive.outputs.0.endpoint
}

output "wazuh_endpoint" {
  description = "The Wazuh Endpoit"
  value       = helm_release.wazuh.outputs.0.endpoint
}