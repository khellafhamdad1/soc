provider "azurerm" {
  version = "=2.40.0"
  features {}
}

# provider "helm" {
#   kubernetes {
#     host                   = azurerm_kubernetes_cluster.aks.kube_config.0.host
#     client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate)
#     client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_key)
#     cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
#   }
# }

# provider "kubernetes" {
#   host                   = azurerm_kubernetes_cluster.aks.kube_config.0.host
#   username               = azurerm_kubernetes_cluster.aks.kube_config.0.username
#   password               = azurerm_kubernetes_cluster.aks.kube_config.0.password
#   client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate)
#   client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_key)
#   cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
# }


provider "helm" {
  kubernetes {
    config_path = azurerm_kubernetes_cluster.aks.kube_config_raw
  }
}

provider "kubernetes" {
  config_path = azurerm_kubernetes_cluster.aks.kube_config_raw
}